from django.urls import path
from .api.veiculo import veiculoViewsets
from .api.publica import publicaViewsets

urlpatterns =[
    path(r'veiculo/veiculo_disponivel',
         veiculoViewsets.VeiculoDisponiveisListViewSet.as_view(),
         name='veiculo_disponivel'),
    path('veiculo/vincula-com-garagem/<int:id>',
         veiculoViewsets.VeiculoUpdateViewSet.as_view(),
         name='vincula-com-garagem'),
    path('veiculo/lista-todos-veiculos',
         veiculoViewsets.VeiculoListViewSet.as_view(),
         name='lista-todos-veiculos'),
    path('publica/pessoas',
         publicaViewsets.PessoasCadastradasListViewSet.as_view(),
         name='lista-pessoas'),
    path('publica/garagem-ativa',
         publicaViewsets.GaragemAtivasListViewSet.as_view(),
         name='garagem-ativa'),
    path('publica/pessoa-sem-veiculo',
         publicaViewsets.PessoaSemVeiculoListViewSet.as_view(),
         name='pessoa-sem-veiculo'),
    path('publica/pessoa-com-veiculo',
         publicaViewsets.PessoaComVeiculoListViewSet.as_view(),
         name='pessoa-com-veiculo')
]
