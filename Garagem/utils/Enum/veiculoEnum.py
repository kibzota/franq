from enum import IntEnum


class TipoVeiculoEnum(IntEnum):
    Carro = 0
    Moto = 1

    @classmethod
    def choices(cls):
        return [(key.value, key.name) for key in cls]