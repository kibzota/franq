from django.db import models
from .utils.Enum.veiculoEnum import TipoVeiculoEnum


class GaragemManager(models.Manager):

    def pessoas_com_veiculos(self):
        return self.filter(pessoa__isnull=False, veiculo__isnull=False).get().pessoa_set

    def pessoas_sem_veiculos(self):
        return self.filter(pessoa__isnull=False, veiculo__isnull=True).get().pessoa_set


class Garagem (models.Model):
    email = models.EmailField()
    telefone = models.CharField(max_length=17)

    objects = GaragemManager()


class Veiculo (models.Model):
    cor = models.CharField(max_length=20)
    ano = models.PositiveIntegerField()
    tipo = models.IntegerField(choices=TipoVeiculoEnum.choices(), default=TipoVeiculoEnum.Carro)
    placa = models.CharField(max_length=15)
    modelo = models.CharField(max_length=50, null=True)
    garagem = models.ForeignKey(Garagem, null=True, on_delete=models.DO_NOTHING)





