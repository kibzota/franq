from rest_framework import permissions, status
from rest_framework.generics import mixins, ListCreateAPIView, GenericAPIView
from Garagem.api.veiculo import veiculoSerializer
from rest_framework.response import Response
from Garagem import models
from Garagem.utils.Enum.veiculoEnum import TipoVeiculoEnum


class VeiculoDisponiveisListViewSet(ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = veiculoSerializer.VeiculoSerializer
    queryset = models.Veiculo.objects

    def get_queryset(self):
        return self.queryset.filter(garagem=None)


class VeiculoListViewSet(GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = veiculoSerializer.VeiculoSerializer
    queryset = models.Veiculo.objects

    def get(self, request):
        veiculos = []
        for veiculo in self.queryset.all():
            if veiculo.tipo == TipoVeiculoEnum.Moto:
                veiculos.append({'modelo': veiculo.modelo, 'ano': veiculo.ano})
            else:
                veiculos.append({'cor': veiculo.cor, 'ano': veiculo.ano})
        return Response(veiculos, status=status.HTTP_200_OK)


class VeiculoUpdateViewSet(mixins.UpdateModelMixin, GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = veiculoSerializer.VeiculoSerializer
    queryset = models.Veiculo.objects
    lookup_field = "id"

    def patch(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.garagem = request.user.garagem
        serializer = self.get_serializer(instance)
        self.partial_update(serializer, *args, **kwargs)
        return Response(serializer.data, status=status.HTTP_200_OK)

