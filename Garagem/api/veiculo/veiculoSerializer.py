from rest_framework import serializers
from Garagem import models


class VeiculoSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Veiculo
        fields = ['id', 'cor', 'modelo','placa', 'tipo', 'ano']
