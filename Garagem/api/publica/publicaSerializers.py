from rest_framework import serializers
from Garagem.models import Veiculo, Garagem
from Pessoa.models import Pessoa


class VeiculoAPIPublicaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Veiculo
        fields = ['cor', 'modelo', 'placa', 'tipo', 'ano']


class PessoaAPIPublicaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pessoa
        fields = ['email', 'nome', 'telefone']


class GaragemAPIPublicaSerializer(serializers.ModelSerializer):
    pessoa = PessoaAPIPublicaSerializer(many=True)


    class Meta:
        model = Garagem
        fields = ['email', 'telefone', 'pessoa']
        depth = 3


