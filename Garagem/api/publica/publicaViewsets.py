from rest_framework import permissions, status
from rest_framework.generics import mixins, ListCreateAPIView, GenericAPIView, UpdateAPIView
from Garagem.api.publica.publicaSerializers import PessoaAPIPublicaSerializer, GaragemAPIPublicaSerializer
from rest_framework.response import Response
from Garagem.models import Garagem
from Pessoa.models import Pessoa


class PessoasCadastradasListViewSet(GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = PessoaAPIPublicaSerializer
    queryset = Pessoa.objects

    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.queryset.all(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class GaragemAtivasListViewSet(GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = GaragemAPIPublicaSerializer
    queryset = Garagem.objects

    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.queryset.filter(pessoa__isnull=False), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class PessoaComVeiculoListViewSet(GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = PessoaAPIPublicaSerializer
    queryset = Garagem.objects

    def get(self, request, *args, **kwargs):
        serializer = PessoaAPIPublicaSerializer(self.queryset.pessoas_com_veiculos(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class PessoaSemVeiculoListViewSet(GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = PessoaAPIPublicaSerializer
    queryset = Garagem.objects

    def get(self, request, *args, **kwargs):
        serializer = PessoaAPIPublicaSerializer(self.queryset.pessoas_sem_veiculos(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
