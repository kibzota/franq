from rest_framework import permissions, status, generics
from Pessoa.api import serializers
from rest_framework.response import Response


class PessoaViewSet(generics.GenericAPIView):
    serializer_class = serializers.PessoaSerializer

    def post(self, request):
        pessoa = request.data
        serializer = self.serializer_class(data=pessoa)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class PessoaComGaragemViewSet(generics.GenericAPIView):
    serializer_class = serializers.PessoaSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)

    def post(self, request):
        pessoa = request.data
        serializer = self.serializer_class(data=pessoa)
        serializer.is_valid(raise_exception=True)
        serializer.save(com_garagem=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class LoginAPIViewSet(generics.GenericAPIView):
    serializer_class = serializers.LoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class LogoutAPIView(generics.GenericAPIView):
    serializer_class = serializers.LogoutSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_204_NO_CONTENT)
