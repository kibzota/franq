from django.db import models
from Garagem.models import Garagem
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin)
from rest_framework_simplejwt.tokens import RefreshToken


class UserManager(BaseUserManager):
    def criar_pessoa(self, **kwargs):
        if kwargs['nome'] is None:
            raise TypeError('Pessoa ter um nome ')

        if kwargs['email'] is None:
            raise TypeError('Pessoa deve ter um Email')

        if kwargs['telefone'] is None:
            raise TypeError('Pessoa deve ter um Telefone')

        pessoa = self.model(
            nome=kwargs['nome'],
            telefone=kwargs['telefone'],
            email=self.normalize_email(kwargs['email']),
            is_staff=True if kwargs['admin'] else False,
            is_superuser=True if kwargs['admin'] else False
        )
        pessoa.set_password(kwargs['password'])
        pessoa.save()
        return pessoa

    def criar_pessoa_com_garagem(self, **kwargs):
        pessoa = self.criar_pessoa(**kwargs)
        garagem = Garagem(email=pessoa.email, telefone=pessoa.telefone)
        garagem.save()
        pessoa.garagem = garagem
        pessoa.save()
        return pessoa


# Create your models here.
class Pessoa (AbstractBaseUser, PermissionsMixin):
    nome = models.CharField(max_length=150)
    email = models.EmailField(unique=True)
    telefone = models.CharField(max_length=17)
    is_staff = models.BooleanField(default=False)
    garagem = models.ForeignKey(Garagem, null=True, on_delete=models.DO_NOTHING)

    USERNAME_FIELD = 'email'

    objects = UserManager()

    def __str__(self):
        return self.nome

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }
