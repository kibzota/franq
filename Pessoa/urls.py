from django.urls import path
from .api import viewsets


urlpatterns =[
    path(r'pessoa/criar', viewsets.PessoaViewSet.as_view(), name='pessoa'),
    path(r'pessoa/criar-pessoa-com-garagem', viewsets.PessoaComGaragemViewSet.as_view(), name='pessoa-com-garagem'),
    path('login/', viewsets.LoginAPIViewSet.as_view(), name='login'),
    path('logout/', viewsets.LogoutAPIView.as_view(), name="logout"),
]
