## Instalação

1. Instale as dependências usando  `pip install -r requirements.txt`

2. Adiciona as informações do banco no arquivo  `.env`

3. Migre as tabelas existentes executando `python manage.py migrate`

4. Inicie o servidor de desenvolvimento usando `python manage.py runserver`


## ToDo:
- [ ] Refatorar código
- [ ] Adicionar mais validaçoes de dados
- [ ] Adicionar Testes